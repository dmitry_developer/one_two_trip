import time
import random
import sys
import string
import redis

from random import randint

HOST = 'localhost'
MS_COUNT = 500

GENERATOR_KEY = 'generator_mq'
MESSAGE_KEY = 'message_mq'
ERROR_KEY = 'error_mq'

GENERATOR_VALUE = 'generator_value'
MESSAGE_LENGTH = 100

server = redis.Redis(host=HOST)


def main():
    if len(sys.argv) > 1 and sys.argv[1] == 'getErrors':
        get_errors()
        return

    while True:
        if generator_is_exists():
            print('consume')
            run_consumer()
        else:
            print('generate')
            run_generator()
        time.sleep(MS_COUNT / 1000)


def get_errors():
    errors = server.lrange(ERROR_KEY, 0, -1)
    for e in errors:
        print(e)


def generator_is_exists():
    return server.set(GENERATOR_KEY,
                      GENERATOR_VALUE,
                      nx=True,
                      px=MS_COUNT) is None


def run_generator():
    server.rpush(MESSAGE_KEY, generate_random_message())


def run_consumer():
    result = server.lpop(MESSAGE_KEY)

    if result is None:
        return

    message = result.decode()
    error = randint(1, 100) <= 5
    if error:
        push_error(message)


def push_error(message):
    server.rpush(ERROR_KEY, message)


def generate_random_message():
    return ''.join([random.choice(string.ascii_letters + string.digits)
                    for _ in range(MESSAGE_LENGTH)])


if __name__ == '__main__':
    main()
