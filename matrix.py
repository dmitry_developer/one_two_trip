def generate_matrix(n):
    matrix = []
    size = 2 * n - 1
    for i in range(1, size + 1):
        matrix.append([])
        for j in range(1, size + 1):
            value = (i - 1) * size + j
            matrix[i - 1].append(value)

    return matrix


def print_matrix(matrix):
    result = []

    size = len(matrix)

    step = 1
    i = j = size // 2
    result.append(matrix[i][j])
    flag = True
    while True:
        # left
        for _ in range(step):
            j -= 1
            if len(result) == size * size:
                flag = False
                break
            result.append(matrix[i][j])
        if not flag:
            break
        # down
        for _ in range(step):
            i += 1
            result.append(matrix[i][j])
        step += 1
        # right
        for _ in range(step):
            j += 1
            result.append(matrix[i][j])
        # up
        for _ in range(step):
            i -= 1
            result.append(matrix[i][j])
        step += 1

    print(' '.join(map(str, result)))



if __name__ == '__main__':
    n = int(input('input n: '))
    matrix = generate_matrix(n)
    print_matrix(matrix)
